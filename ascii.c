#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define REP(i, k) for(int i=0; i<k; i++)


int main(){
    int i;
    for(i=0; i<256; i++){ 
        printf("%d: %c ", i, i);
        if(i%16 == 15) printf("\n");
    }
}