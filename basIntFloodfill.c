#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <time.h>

#define REP(i, k) for(int i=0; i<k; i++)
#define REPP(i, j, k) REP(i, k) REP(j, k)
#define GRT(i) (i >= 0)         // Boundary handling
#define LST(i) (i < WIDTH)      // Boundary handling
#define WIDTH 10

int Board[WIDTH][WIDTH];

void GotoXY(SHORT x, SHORT y){
    COORD K = {x, y};
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), K);
}

void SetColor(SHORT color){
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}

void Box(SHORT x, SHORT y, SHORT id){
    SHORT KoorX = (6*x) + 1;
    SHORT KoorY = (3*y) + 1;

    SetColor(id);
    if(id < 0) SetColor(0);
    GotoXY(KoorX, KoorY+0); printf("%c%c%c%c%c", 222, 219, 219, 219, 221);
    GotoXY(KoorX, KoorY+1); printf("%c%c%c%c%c", 222, 219, 219, 219, 221);
}

void SelectionBox(SHORT x, SHORT y){
    SHORT KoorX = 6*x;    
    SHORT KoorY = 3*y;

    SetColor(9);
    GotoXY(KoorX, KoorY+0);                                              printf("%c%c%c%c%c%c%c\n",  201, 205, 205, 205, 205, 205, 187);
    GotoXY(KoorX, KoorY+1); printf("%c", 186); GotoXY(KoorX+6, KoorY+1); printf(            "%c\n",                                186);
    GotoXY(KoorX, KoorY+2); printf("%c", 186); GotoXY(KoorX+6, KoorY+2); printf(            "%c\n",                                186);
    GotoXY(KoorX, KoorY+3);                                              printf("%c%c%c%c%c%c%c\n",  200, 205, 205, 205, 205, 205, 188);
}

void ClearSelectionBox(SHORT x, SHORT y){
    SHORT KoorX = 6*x;    
    SHORT KoorY = 3*y;

    GotoXY(KoorX, KoorY+0);                                             printf("%c%c%c%c%c%c%c\n", 32, 32, 32, 32, 32, 32, 32);
    GotoXY(KoorX, KoorY+1); printf("%c", 32); GotoXY(KoorX+6, KoorY+1); printf(            "%c\n",                         32);
    GotoXY(KoorX, KoorY+2); printf("%c", 32); GotoXY(KoorX+6, KoorY+2); printf(            "%c\n",                         32);
    GotoXY(KoorX, KoorY+3);                                             printf("%c%c%c%c%c%c%c\n", 32, 32, 32, 32, 32, 32, 32);
}

void SelectedSelectionBox(SHORT x, SHORT y){
    SHORT KoorX = 6*x;    
    SHORT KoorY = 3*y;  

    SetColor(15);
    GotoXY(KoorX, KoorY+0);                                              printf("%c%c%c%c%c%c%c\n",  218, 196, 196, 196, 196, 196, 191);
    GotoXY(KoorX, KoorY+1); printf("%c", 179); GotoXY(KoorX+6, KoorY+1); printf(            "%c\n",                                179);
    GotoXY(KoorX, KoorY+2); printf("%c", 179); GotoXY(KoorX+6, KoorY+2); printf(            "%c\n",                                179);
    GotoXY(KoorX, KoorY+3);                                              printf("%c%c%c%c%c%c%c\n",  192, 196, 196, 196, 196, 196, 217);
}

void GenerateRandomBoard(){
    srand(time(0));

    REPP(i, j, WIDTH){
        Board[i][j] = ((i+j)*rand())%5 + 1;
    }
}

void GenerateBox(){
    REPP(i, j, WIDTH){
        Box(i, j, Board[i][j]);
    }
}

void GenerateRandomBox(){
    GenerateRandomBoard();
    GenerateBox();
}

void SwapInt(int &a, int &b){
    int t = b;
    b = a;
    a = t;
}

int PreSwap(SHORT x, SHORT y){
    SelectedSelectionBox(x, y);

    SHORT SwappedCursorX = x;
    SHORT SwappedCursorY = y;

    fflush(stdin);

    int Ret = 0;

    if(getch() == 224){
        switch(getch()){
            case 75:
                if(!SwappedCursorX) break;          // Left Arrow 
                SwappedCursorX--;
                Ret = 1;
                break;
            case 72:
                if(!SwappedCursorY) break;          // Up Arrow 
                SwappedCursorY--;
                Ret = 2;
                break;
            case 77:
                if(SwappedCursorX == WIDTH-1) break;      // Right Arrow 
                SwappedCursorX++;
                Ret = 3;
                break;
            case 80:
                if(SwappedCursorY == WIDTH-1) break;      // Down Arrow 
                SwappedCursorY++;
                Ret = 4;
                break;
        }
    }

    SwapInt(Board[x][y], Board[SwappedCursorX][SwappedCursorY]);

    Box(x, y, Board[x][y]);
    Box(SwappedCursorX, SwappedCursorY, Board[SwappedCursorX][SwappedCursorY]);

    ClearSelectionBox(x, y);
    SelectionBox(SwappedCursorX, SwappedCursorY);

    return Ret;
}

int FloodFill(char flood, char repl, int index){
    int i = index/WIDTH;
    int j = index%WIDTH;

    if(Board[i][j] != flood) return 0;

    Board[i][j] = repl;

    int UP      = 0;
    int RIGHT   = 0;
    int DOWN    = 0;
    int LEFT    = 0;
    int HERE    = 1;

    if(GRT(i-1)){        // Atas
        UP = FloodFill(flood, repl, index-WIDTH);
    }
    if(LST(j+1)){        // Kanan
        RIGHT = FloodFill(flood, repl, index+1);
    }
    if(LST(i+1)){        // Bawah
        DOWN = FloodFill(flood, repl, index+WIDTH);
    }
    if(GRT(j-1)){        // Kiri
        LEFT = FloodFill(flood, repl, index-1);
    }


    return UP+RIGHT+DOWN+LEFT+HERE;
}

int BoundedFloodFill(char flood, char repl, int index, int boundary){
    int i = index/WIDTH;
    int j = index%WIDTH;

    if(Board[i][j] != flood) return 0;

    int ret = FloodFill(flood, repl, index);

    if(ret < boundary) FloodFill(repl, flood, index);

    return ret;
}

void Runtuh(){
    int i, j, k;
    
    REPP(i, j, WIDTH) {
        if(Board[i][j] < 0){
            k=i;
            while(k > 0){
                SwapInt(Board[k][j], Board[k-1][j]);
                k--;
            }
        }
    }
}

void Movement(){
    char Key;   
    SHORT SelectionCursorX = 0;
    SHORT SelectionCursorY = 0;
    SHORT Pressed = 0;

    while(1){
        GotoXY(0, 3*8 + 1);
        switch(getch()){
            case 32:
                Pressed ^= 1;
                break;
            case 'g':                           // For Testing
                GenerateRandomBox();
                break;
            case 224:
                switch(getch()){
                    case 75:
                        ClearSelectionBox(SelectionCursorX, SelectionCursorY);
                        if(SelectionCursorX == 0)  break;
                        SelectionCursorX--;
                        break;
                    case 72:
                        ClearSelectionBox(SelectionCursorX, SelectionCursorY);
                        if(SelectionCursorY == 0) break;
                        SelectionCursorY--;
                        break;
                    case 80:
                        ClearSelectionBox(SelectionCursorX, SelectionCursorY);
                        if(SelectionCursorY == WIDTH-1) break;
                        SelectionCursorY++;
                        break;
                    case 77:
                        ClearSelectionBox(SelectionCursorX, SelectionCursorY);
                        if(SelectionCursorX == WIDTH-1) break;
                        SelectionCursorX++;
                        break;
                break;
            }
        }

        SelectionBox(SelectionCursorX, SelectionCursorY);

        if(Pressed){
            ClearSelectionBox(SelectionCursorX, SelectionCursorY);

            switch(PreSwap(SelectionCursorX, SelectionCursorY)){
                case 1:
                    SelectionCursorX--;
                    break;
                case 2:
                    SelectionCursorY--;
                    break;
                case 3:
                    SelectionCursorX++;
                    break;
                case 4:
                    SelectionCursorY++;
                    break;
                case 0:
                    break;
            }

            SelectionBox(SelectionCursorX, SelectionCursorY);
            Pressed ^= 1;
        }
    }
}

int main(){
    system("cls");

    GenerateRandomBox();

    FloodFill(1, -1, 1);

    if(getch()){
        GenerateBox();
    }
}