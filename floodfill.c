#include <stdio.h>

#define REP(i, k) for(int i=0; i<k; i++)
#define REPP(i, j, k) REP(i, k) REP(j, k)
#define GRT(i) (i >= 0)         // Boundary handling
#define LST(i) (i < WIDTH)      // Boundary handling

#define WIDTH 8

char Board[WIDTH][WIDTH];

void printingBoard();                                                           // [x]
int floodfill(char flood, char repl, int index);                                // [x]
int boundedFloodfill(char flood, char repl, int index, int boundary);           // [x]

int main(){
    REPP(i, j, WIDTH) scanf("%c ", &Board[i][j]);

    REPP(i, j, WIDTH) boundedFloodfill('3', '*', (i*8)+j, 3);

    printingBoard();
}

void printingBoard(){
    REP(i, WIDTH){
        REP(j, WIDTH){
            printf("%c ", Board[i][j]);
        }
        printf("\n");
    }
}

int floodfill(char flood, char repl, int index){
    int i = index/8;
    int j = index%8;

    if(Board[i][j] != flood) return 0;

    Board[i][j] = repl;

    int UP      = 0;
    int RIGHT   = 0;
    int DOWN    = 0;
    int LEFT    = 0;
    int THIS    = 1;

    if(GRT(i-1)){        // Atas
        UP = floodfill(flood, repl, index-8);
    }
    if(LST(j+1)){        // Kanan
        RIGHT = floodfill(flood, repl, index+1);
    }
    if(LST(i+1)){        // Bawah
        DOWN = floodfill(flood, repl, index+8);
    }
    if(GRT(j-1)){        // Kiri
        LEFT = floodfill(flood, repl, index-1);
    }


    return UP+RIGHT+DOWN+LEFT+THIS;
}

int boundedFloodfill(char flood, char repl, int index, int boundary){
    int i = index/8;
    int j = index%8;

    if(Board[i][j] != flood) return 0;

    int ret = floodfill(flood, repl, index);

    if(ret < boundary) floodfill(repl, flood, index);

    return ret;
}