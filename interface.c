#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define REP(i, k) for(int i=0; i<k; i++)

void GotoXY(SHORT x, SHORT y){
    COORD K = {x, y};
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), K);
}

void Box(SHORT x, SHORT y){
    SHORT KoorX = (6*x) + 1;
    SHORT KoorY = (3*y) + 1;

    GotoXY(KoorX, KoorY+0); printf("%c%c%c%c%c", 222, 219, 219, 219, 221);
    GotoXY(KoorX, KoorY+1); printf("%c%c%c%c%c",  222, 219, 219, 219, 221);
}

void SelectionBox(SHORT x, SHORT y){
    SHORT KoorX = 6*x;    
    SHORT KoorY = 3*y;

    GotoXY(KoorX, KoorY);                                                printf("%c%c%c%c%c%c%c\n",  201, 205, 205, 205, 205, 205, 187);
    GotoXY(KoorX, KoorY+1); printf("%c", 186); GotoXY(KoorX+6, KoorY+1); printf(            "%c\n",       186, 186);
    GotoXY(KoorX, KoorY+2); printf("%c", 186); GotoXY(KoorX+6, KoorY+2); printf(            "%c\n",       186, 186);
    GotoXY(KoorX, KoorY+3);                                              printf("%c%c%c%c%c%c%c\n",  200, 205, 205, 205, 205, 205, 188);
}

void ClearSelectionBox(SHORT x, SHORT y){
    SHORT KoorX = 6*x;    
    SHORT KoorY = 3*y;

    GotoXY(KoorX, KoorY);                                               printf("%c%c%c%c%c%c%c\n", 32, 32, 32, 32, 32, 32, 32);
    GotoXY(KoorX, KoorY+1); printf("%c", 32); GotoXY(KoorX+6, KoorY+1); printf(            "%c\n",                     32, 32);
    GotoXY(KoorX, KoorY+2); printf("%c", 32); GotoXY(KoorX+6, KoorY+2); printf(            "%c\n",                     32, 32);
    GotoXY(KoorX, KoorY+3);                                             printf("%c%c%c%c%c%c%c\n", 32, 32, 32, 32, 32, 32, 32);
}

void EmptyNewline(SHORT X){
    REP(i, 3*X) printf("\n");
}

int main(){
    system("cls");

    REP(i, 8) REP(j, 8) Box(i, j);

    ClearSelectionBox(1, 1);
    SelectionBox(1, 3); 
    EmptyNewline(4);
}