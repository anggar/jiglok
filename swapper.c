#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REP(i, k) for(int i=0; i<k; i++)
#define REPP(i, j, k) REP(i, k) REP(j, k)
#define GRT(i) (i >= 0)         // Boundary handling
#define LST(i) (i < WIDTH)      // Boundary handling

#define WIDTH 10

char Board[WIDTH][WIDTH];

void swap(char & a, char & b){
    char temp = b;
    b = a;
    a = temp;
}

void printingBoard();                                                           // [x]

int main(){
    FILE * BoardFile;
    BoardFile = fopen("board.in", "r");

    REPP(i, j, WIDTH) fscanf(BoardFile, "%c ", &Board[i][j]);

    printingBoard();

    int i, j;
    int dir;
    while(1){
        printf("Input the index : ");
        scanf("%d %d", &i, &j);

        printf("Select the direction : ");
        scanf("%d", &dir);

        if((dir == 1) && (GRT(i-1))) swap(Board[i][j], Board[i-1][j]);
        if((dir == 2) && (LST(j+1))) swap(Board[i][j], Board[i][j+1]);
        if((dir == 3) && (LST(i+1))) swap(Board[i][j], Board[i+1][j]);
        if((dir == 4) && (GRT(j-1))) swap(Board[i][j], Board[i][j-1]);

        printingBoard();
    }
}

void printingBoard(){
    REP(i, WIDTH){
        REP(j, WIDTH){
            printf("%c ", Board[i][j]);
        }
        printf("\n");
    }

    printf("\n");
}